﻿// HW19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void Voice(){};
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow" << endl;
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "Moo" << endl;
    }
};

int main()
{
    const int size = 3;
    Animal* animals[size] = { new Dog, new Cat, new Cow };
    for (Animal* p : animals)
        p->Voice();
}

